`use strict`;
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = [
  {
    entry: {
      index: `./src/index.tsx`,
      vendor: [`rxjs`, `react`, `react-dom`]
    },
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'js/[name].js'
    },
    externals: {
      "jquery": "jQuery"
    },
    devtool: 'source-map',
    devServer: {
      contentBase: path.join(__dirname, 'dist'),
      host: "0.0.0.0"
    },
    resolve: {
      // root: path.resolve(__dirname, `src`),
      extensions: [`.ts`, `.tsx`, `.js`, `scss`, `css`, `.json`]
    },

    module: {
      loaders: [
        { test: /\.tsx?$/, loader: `ts-loader` },
        {
          test: /\.scss$/,
          // loader: ExtractTextPlugin.extract({ use: "css-loader?sourceMap!sass-loader?sourceMap", fallback: "style-loader" })
          use: [{
            loader: "style-loader" // creates style nodes from JS strings
          }, {
            loader: "css-loader" // translates CSS into CommonJS
          }, {
            loader: "sass-loader" // compiles Sass to CSS
          }]
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2)$/,
          loader: 'file-loader?name=/fonts/[name].[ext]'
        }
      ],
      noParse: /jquery|lodash/
    },
    plugins: [
      new webpack.optimize.CommonsChunkPlugin({ names: [`vendor`, 'manifest'] }),
      new ExtractTextPlugin('css/styles.css')
    ]
  }
]