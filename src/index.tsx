import "./index.scss"

import * as React from "react"
import * as ReactDOM from "react-dom"


class Hello extends React.Component<{},{}> {
  constructor() {
    super()
  }
  render () {
    return <h1>Hello!!</h1>
  }
}

$(document).ready( () => {
  console.log($(`#react-root`)[0])
  ReactDOM.render(<Hello />, $(`#react-root`)[0], () => console.log(`all good!`)) 
})


